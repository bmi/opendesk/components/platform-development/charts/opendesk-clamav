{{/* vim: set filetype=mustache: */}}
{{/*
   * SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat,
   *                              PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
   *
   * SPDX-License-Identifier: Apache-2.0
   */}}
{{/*
   * Basic Auth string.
   *
   * Usage: {{ include "clamav.basicAuthString" ( dict "item" .Values.settings.freshclam.database.mirror "root" $ ) }}
   */}}
{{- define "clamav.basicAuthString" -}}
{{- $auth := .root.Values.settings.freshclam.database.auth }}
{{- if .item.auth }}
  {{- $auth := .item.auth }}
{{- end }}

{{- if $auth }}
  {{- printf "%s:%s@" $auth.username $auth.password }}
{{- end }}
{{- end -}}
